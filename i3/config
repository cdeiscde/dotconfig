# i3 config file
# vim:fdm=marker

# General {{{
set $mod Mod4

# text rendering and scalability on retina/hidpi displays (thanks to pango).
font pango:Monospace 8

# Split hori by default
default_orientation horizontal
workspace_layout default

# Auto back and forth
workspace_auto_back_and_forth no
bindsym $mod+Tab workspace back_and_forth

# follow the focus
focus_follows_mouse no

# Hide edge and title bar
new_window pixel 2
new_float pixel 2

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
# bindsym $mod+Return exec xfce4-terminal
# bindsym $mod+Return exec --no-startup-id terminator
bindsym $mod+Return exec --no-startup-id urxvt
# bindsym $mod+Return exec --no-startup-id termite

# kill focused window
bindsym $mod+Shift+q kill

# start rofi (a program launcher)
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop --dmenu='rofi -show run'
bindsym $mod+d exec "rofi -show-icons -modi combi -show combi -combi-modi run,drun"
bindsym $mod+Shift+p exec "rofi -show window"

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle
bindsym $mod+Shift+f fullscreen toggle global

# stick mode for the focused container
bindsym $mod+g sticky toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+z focus child

bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# }}}

# Binding Keys {{{

bindsym $mod+F1 exec --no-startup-id ~/.config.d/monitor.sh
bindsym $mod+F2 exec --no-startup-id pavucontrol

# Moving between monitors
bindsym $mod+m move workspace to output right
bindsym $mod+n move workspace to output left

# }}}

# Mode {{{

# resize window (you can also use the mouse for that)
mode "" {
	bindsym j resize shrink width 10 px or 10 ppt
	bindsym k resize grow height 10 px or 10 ppt
	bindsym l resize shrink height 10 px or 10 ppt
	bindsym semicolon resize grow width 10 px or 10 ppt

	# back to normal: Enter or Escape
	bindsym Return mode "default"
	bindsym Escape mode "default"
}
bindsym $mod+r mode ""

for_window [class="[.]*"] floating enable
for_window [class="Matplotlib"] floating enable
for_window [title="Telegram"] floating enable
for_window [class="VirtualBox"] floating enable, resize set 800 600
for_window [instance="pavucontrol"] floating enable
for_window [class="Thunar"] floating enable,resize set 880 640

set $mode_system "L|S|P|R"

mode $mode_system {
	bindsym l exec --no-startup-id i3lock -c 000000, mode "default"
	bindsym s exec --no-startup-id systemctl suspend, mode "default"
	bindsym p exec --no-startup-id systemctl poweroff -i, mode "default"
	bindsym r exec --no-startup-id systemctl reboot, mode "default"
	bindsym e exec --no-startup-id i3-msg exit, mode "default"

	bindsym Return mode "default"
	bindsym Escape mode "default"
}
bindsym $mod+x mode $mode_system

# }}}

# Autostart {{{

exec_always --no-startup-id fcitx
exec_always --no-startup-id feh --bg-scale ~/Images/bg.jpg
exec_always --no-startup-id nm-applet
exec_always --no-startup-id blueman-applet

# }}}

# Media Key {{{

bindsym XF86AudioPlay exec audtool playback-playpause
bindsym XF86AudioStop exec audtool playback-stop
bindsym XF86AudioNext exec audtool playlist-advance
bindsym XF86AudioPrev exec audtool playlist-reverse

bindsym XF86AudioRaiseVolume exec pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume exec pactl set-sink-volume 0 -5%
bindsym XF86AudioMute exec pactl set-sink-mute 0 toggle

# Brightness
bindsym XF86MonBrightnessUp exec xbacklight -inc 5
bindsym XF86MonBrightnessDown exec xbacklight -dec 5

# }}}

# Color {{{

set $back				#1d1f21
set $black			#282a2e
set $grey				#373b41
set $lightgrey	#707880
set $white			#c5c8c6
set $yellow			#f0c674
set $red				#cc6666
set $darkred		#a54242
set $green			#b5bd56
# Layout colors					board			bg				text				indicator
client.focused					$green		$green		$black			$green
client.focused_inactive	$grey			$grey			$lightgrey	$grey
client.unfocused				$grey			$grey			$lightgrey	$grey
client.urgent						$red			$red			$black			$red
client.background				$back

# }}}

# Bar {{{

bar {
	status_command i3status -c ~/.config/i3/i3status.conf
	font pango:DejaVu Sans Mono, FontAwesome 8
	position top
	mode hide
	hidden_state hide
	modifier Mod4
	separator_symbol "/"
# Color panel
	colors {
		background	$black
		statusline	$lightgrey
		separator		$lightgrey
#   Indicadores panel
#												border		bg					text
		focused_workspace		$black		$green			$black
		active_workspace		$black		$lightgrey	$black
		inactive_workspace	$black		$grey				$green
		urgent_workspace		$black		$darkred		$red
	}
}

# }}}
